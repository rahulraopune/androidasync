package com.example.yashbeproject.steganography;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import android.os.Environment;

public class CopyToBankDir
{
	public static String copytobankdir(String path,String username) throws Exception
	{
		InputStream inStream = null;
		OutputStream outStream = null;
		
		File f = new File(path);
		int lastSlash = f.getAbsolutePath().lastIndexOf("/");
		String basename = f.getAbsolutePath().substring(lastSlash+1,f.getAbsolutePath().length());
		System.out.println(basename);
		int lastDOT = basename.lastIndexOf(".");
		String extension=basename.substring(lastDOT+1,basename.length());
		String newfilename = username+"."+extension;
		String newpath = Environment.getExternalStorageDirectory()+"/Bank/"+newfilename;
		File fnew = new File(newpath);
		 inStream = new FileInputStream(f);
 	    outStream = new FileOutputStream(fnew);
     	
 	    byte[] buffer = new byte[1024];
 		
 	    int length;
 	    //copy the file content in bytes 
 	    while ((length = inStream.read(buffer)) > 0){
 	  
 	    	outStream.write(buffer, 0, length);
 	 
 	    }
 	 
 	    inStream.close();
 	    outStream.close();
 	    return newpath;
	}
}
