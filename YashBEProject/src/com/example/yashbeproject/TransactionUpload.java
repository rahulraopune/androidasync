package com.example.yashbeproject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.yashbeproject.config.AppURLConfig;
import com.example.yashbeproject.steganography.CopyToBankDir;

public class TransactionUpload extends Activity implements OnClickListener
{
	private static final int SELECT_IMAGE = 1;
	Button bTransactionSelectImage;
	Button bTransactionUploadSelectedImage;
	TextView tvTransactionPath;
	
	public String credituserid = "";
	public int creditamount;
	public String path="";
	public String newpath="";
	public String transactionfilename="";
	
	public String username = "";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_transaction_upload);
		
		bTransactionSelectImage = (Button)findViewById(R.id.bTransactionSelectImage);
		bTransactionSelectImage.setOnClickListener(this);
		bTransactionUploadSelectedImage = (Button)findViewById(R.id.bTransactionUploadImage);
		bTransactionUploadSelectedImage.setOnClickListener(this);
		tvTransactionPath = (TextView)findViewById(R.id.tvTransactionPath);
		tvTransactionPath.setText("");
		
		username = getIntent().getExtras().getString("username");
		credituserid = getIntent().getExtras().getString("credituserid");
		creditamount = getIntent().getExtras().getInt("creditamount");
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
		case R.id.bTransactionSelectImage:
			Intent imageselectIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			startActivityForResult(imageselectIntent, SELECT_IMAGE);
			break;
			
		case R.id.bTransactionUploadImage:
			try {
				transactionfilename = "transaction_"+String.valueOf(new Random().nextInt(34123));
				newpath = CopyToBankDir.copytobankdir(path,transactionfilename);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//async task
			TransactionUploadImageAsyncTask u = new TransactionUploadImageAsyncTask();
			u.execute(new String[]{AppURLConfig.BASIC_SERVER_URL+"/BankServer/TransactionUpload"});
			break;
		}
	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) { 
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent); 

        switch(requestCode) { 
        case SELECT_IMAGE:
            if(resultCode == RESULT_OK){
            	Uri selectedImage = imageReturnedIntent.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                path = cursor.getString(columnIndex);
                tvTransactionPath.setText(path);
                cursor.close();
                

            }
        }
    }
	
	private class TransactionUploadImageAsyncTask extends AsyncTask<String, Void, String>
    {

        @Override
        protected String doInBackground(String... urls) 
        {
        	
            HttpURLConnection httpConn=null;

            try
            {
            String file = newpath;
            File uploadFile = new File(file);
            FileInputStream inputStream = new FileInputStream(uploadFile);

            //System.out.println("File to upload: " + file);

            // creates a HTTP connection
            URL url1 = new URL(urls[0]);
            httpConn = (HttpURLConnection) url1.openConnection();
            httpConn.setUseCaches(false);
            httpConn.setDoOutput(true);
            httpConn.setRequestMethod("POST");
            httpConn.setRequestProperty("fileName", uploadFile.getName());
            httpConn.connect();
            // sets file name as a HTTP header

            Log.i("fileName", uploadFile.getName());
            // opens output stream of the HTTP connection for writing data

            OutputStream outputStream = httpConn.getOutputStream();

            // Opens input stream of the file for reading data


            byte[] buffer = new byte[4096];
            int bytesRead = -1;

            System.out.println("Start writing data...");

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            System.out.println("Data was written.");
            outputStream.close();
            inputStream.close();
            }
            catch(SocketTimeoutException e)
            {
                 Log.e("Debug", "error: " + e.getMessage(), e);
            }
            catch (MalformedURLException ex)
             {
                  Log.e("Debug", "error: " + ex.getMessage(), ex);
             }
             catch (IOException ioe)
             {
                  Log.e("Debug", "error: " + ioe.getMessage(), ioe);
             }

            try
            {

            // always check HTTP response code from server
            int responseCode = httpConn.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                // reads server's response
                BufferedReader reader = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
                String response = reader.readLine();
                System.out.println("Server's response: " + response);
            } else {
                System.out.println("Server returned non-OK code: " + responseCode);
            }
            }
             catch (IOException ioex){
                  Log.e("Debug", "error: " + ioex.getMessage(), ioex);
             }
            return null;
        }
        @Override
        protected void onPostExecute(String result) {             
        	Toast.makeText(TransactionUpload.this	,path+" Transaction Image Uploaded Successfully", Toast.LENGTH_LONG).show();
        	Intent i = new Intent(TransactionUpload.this,TransactionProcess.class);
        	i.putExtra("username", username);
        	i.putExtra("transactionfilename",transactionfilename);
        	i.putExtra("credituserid",getIntent().getExtras().getString("credituserid"));
        	i.putExtra("creditamount",getIntent().getExtras().getInt("creditamount"));
        	startActivity(i);
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onProgressUpdate(Void... values) {
        }
    }
	
	
}
