package com.example.yashbeproject.config;

public class AppURLConfig 
{
	public static final String BASIC_SERVER_PROTOCOL="http";
	public static final String BASIC_SERVER_IP="192.168.1.4";
	public static final String BASIC_SERVER_PORT="8080";
	public static final String BASIC_SERVER_URL=BASIC_SERVER_PROTOCOL+"://"+BASIC_SERVER_IP+":"+BASIC_SERVER_PORT+"/";
}

