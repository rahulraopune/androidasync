package com.example.yashbeproject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class TransactionDetails extends Activity implements OnClickListener{
	EditText etCreditUserId;
	EditText etCreditAmount;
	Button bCreditConfirm;
	
	public String credituserid = "";
	public int creditamount ;
	
	public String username = "";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_transaction_details);
		
		etCreditUserId = (EditText)findViewById(R.id.etCreditUserId);
		etCreditAmount = (EditText)findViewById(R.id.etCreditAmount);
		bCreditConfirm = (Button)findViewById(R.id.bCreditConfirm);
		bCreditConfirm.setOnClickListener(this);
		
		username = getIntent().getExtras().getString("username");
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
		case R.id.bCreditConfirm:
			credituserid = etCreditUserId.getText().toString();
			creditamount = Integer.parseInt(etCreditAmount.getText().toString());
			Intent i = new Intent(TransactionDetails.this,TransactionUpload.class);
			i.putExtra("username", username);
			i.putExtra("credituserid",credituserid);
			i.putExtra("creditamount", creditamount);
			startActivity(i);
			break;
		}
	}
}
