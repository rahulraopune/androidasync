package com.example.yashbeproject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity implements OnClickListener
{
	EditText etUsername;
	EditText etPassword;
	Button bLogin;
	
	String username,password;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etUsername = (EditText)findViewById(R.id.etUsername);
        etPassword = (EditText)findViewById(R.id.etPassword);
        bLogin = (Button)findViewById(R.id.bLogin);
        bLogin.setOnClickListener(this);
        
    }


	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		
		
		username = etUsername.getText().toString();
		password = etPassword.getText().toString();
		
		etUsername.setText("");
		etPassword.setText("");
		
		Intent i = new Intent(this,Upload.class);
		i.putExtra("username", username);
		i.putExtra("password", password);
		startActivity(i);
		
	}
	
	
	
	
    
}
