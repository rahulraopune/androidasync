package com.example.yashbeproject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.example.yashbeproject.config.AppURLConfig;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

public class TransactionProcess extends Activity
{
	public String transactionfilename = "";
	public String credituserid = "";
	public int creditamount = 0;
	
	public String res = "";
	
	public String username = "";
	
	TextView tvTransactionMessage;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_transaction_process);
		
		tvTransactionMessage = (TextView)findViewById(R.id.tvTransactionMsg);
		
		username = getIntent().getExtras().getString("username");
		credituserid = getIntent().getExtras().getString("credituserid");
		creditamount =  getIntent().getExtras().getInt("creditamount");
		transactionfilename = getIntent().getExtras().getString("transactionfilename");
		System.out.println(credituserid+" "+creditamount+" "+transactionfilename);
		EncodingTransactionImage e = new EncodingTransactionImage();
		e.execute();
	}
	
	private class EncodingTransactionImage extends AsyncTask<Void, Void, Void>
	{
		String SERVER_URL = AppURLConfig.BASIC_SERVER_URL+"BankServer/TransactionProcess?transactionfilename="+transactionfilename+"&credituserid="+credituserid+"&creditamount="+creditamount+"&init="+username;
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			try
			{

				HttpGet httpGet = new HttpGet(SERVER_URL);
			
				DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
				
				HttpResponse httpResponse = defaultHttpClient.execute(httpGet);
				
				HttpEntity httpEntity = httpResponse.getEntity();
				
				res = EntityUtils.toString(httpEntity);
				
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) 
		{
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			StringTokenizer st = new StringTokenizer(res,"&");
			List<String> l = new ArrayList<String>();
			while(st.hasMoreTokens())
			{
				l.add(st.nextToken());
			}
			tvTransactionMessage.setText("creditUserId:"+l.get(0)+"\ncreditamount:"+l.get(1));
			new Handler().postDelayed(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					Intent i = new Intent(TransactionProcess.this,TransactionResult.class);
					i.putExtra("username",username);
					startActivity(i);
				}
			},4000);
		}
		
		
	}
}
