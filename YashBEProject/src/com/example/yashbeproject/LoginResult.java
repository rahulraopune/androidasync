package com.example.yashbeproject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.example.yashbeproject.config.AppURLConfig;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class LoginResult extends Activity implements OnClickListener {

	TextView tvWelcomeUser;
	TextView tvUid,tvAccountNo,tvFname,tvLname,tvPhone,tvUserId,tvPassword,tvEmailID,tvBalance;
	Button bInitiateTransaction,bExit;
	public String username = "";
	public String password = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_result);
        
        tvWelcomeUser = (TextView)findViewById(R.id.tvWelcomeUser);
        
        
        tvUid = (TextView)findViewById(R.id.tvUid);
        tvAccountNo = (TextView)findViewById(R.id.tvAccountNumber);
        tvFname = (TextView)findViewById(R.id.tvFname);
        tvLname = (TextView)findViewById(R.id.tvLname);
        tvPhone = (TextView)findViewById(R.id.tvPhone);
        tvUserId = (TextView)findViewById(R.id.tvUserID);
        tvPassword = (TextView)findViewById(R.id.tvPassword);
        tvEmailID = (TextView)findViewById(R.id.tvEmailID);
        tvBalance = (TextView)findViewById(R.id.tvBalance);
        
        bExit = (Button)findViewById(R.id.bExit);
        bExit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				finish();
				System.exit(0);
			}
		});
        
        bInitiateTransaction = (Button)findViewById(R.id.bInitiateTransation);
        bInitiateTransaction.setVisibility(Button.INVISIBLE);
        
        AsyncHttpRequest a = new AsyncHttpRequest();
		a.execute();
		
		
        
        bInitiateTransaction.setOnClickListener(this);
    }


	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		Intent i = new Intent(this,TransactionDetails.class);
		i.putExtra("username", username);
		i.putExtra("password", password);
		startActivity(i);
		
	}
	
	private class AsyncHttpRequest extends AsyncTask<Void, Void, Void>
	{
		
		
		String res = "";
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			Intent i = getIntent();
			username = i.getExtras().getString("username");
			password = i.getExtras().getString("password");
			String SERVER_URL = AppURLConfig.BASIC_SERVER_URL+"BankServer/Login?username="+username+"&password="+password;
			Log.d("server_url",SERVER_URL);
			try
			{

				HttpGet httpGet = new HttpGet(SERVER_URL);
			
				DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
				
				HttpResponse httpResponse = defaultHttpClient.execute(httpGet);
				
				HttpEntity httpEntity = httpResponse.getEntity();
				
				res = EntityUtils.toString(httpEntity);
				
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(!res.equals("Fail"))
			{
				StringTokenizer stringTokenizer = new StringTokenizer(res," ");
				List<String> l = new ArrayList<String>();
				while (stringTokenizer.hasMoreTokens())
				{
					l.add(stringTokenizer.nextToken());
				}
				tvWelcomeUser.setText("Welcome Mr/Mrs "+l.get(2)+" "+l.get(3)+" for e-Banking");
				tvUid.setText("UID: "+l.get(0));
		        tvAccountNo.setText("Account No: "+l.get(1));
		        tvFname.setText("FirstName: "+l.get(2));
		        tvLname.setText("LastName: "+l.get(3));
		        tvPhone.setText("Phone: "+l.get(4));
		        tvUserId.setText("UserId: "+l.get(5));
		        tvPassword.setText("Password: "+l.get(6));
		        tvEmailID.setText("EmailID: "+l.get(7));
		        tvBalance.setText("Account Balance: "+l.get(8));
		        
		        bInitiateTransaction.setVisibility(Button.VISIBLE);
			}
			if(res.equals("Fail"))
			{
				tvWelcomeUser.setText("No User Found,Please check Username and Password");
				tvUid.setText("");
		        tvAccountNo.setText("");
		        tvFname.setText("");
		        tvLname.setText("");
		        tvPhone.setText("");
		        tvUserId.setText("");
		        tvPassword.setText("");
		        tvEmailID.setText("");
		        tvBalance.setText("");
		        
		        bInitiateTransaction.setVisibility(Button.INVISIBLE);
			}
		}
		
	}
}