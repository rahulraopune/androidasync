package com.example.yashbeproject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.example.yashbeproject.config.AppURLConfig;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

public class DecodeImageResult extends Activity
{
	public String username="";
	public String password="";
	
	TextView tvDecodeResult;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_decode_result);
		tvDecodeResult = (TextView)findViewById(R.id.tvDecodeResult);
		
		username = getIntent().getExtras().getString("username");
		password = getIntent().getExtras().getString("password");
		
		
		AsyncHttpRequest a = new AsyncHttpRequest();
		a.execute();
		
		
		
		
		
	}
	
	private class AsyncHttpRequest extends AsyncTask<Void, Void, Void>
	{
		
		
		String res = "";
		
		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			
			String SERVER_URL = AppURLConfig.BASIC_SERVER_URL+"BankServer/DecodeImage?username="+username;
			Log.d("server_url",SERVER_URL);
			try
			{

				HttpGet httpGet = new HttpGet(SERVER_URL);
			
				DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
				
				HttpResponse httpResponse = defaultHttpClient.execute(httpGet);
				
				HttpEntity httpEntity = httpResponse.getEntity();
				
				res = EntityUtils.toString(httpEntity);
				
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(!res.equals("Fail"))
			{
				StringTokenizer stringTokenizer = new StringTokenizer(res,"&");
				List<String> l = new ArrayList<String>();
				while (stringTokenizer.hasMoreTokens())
				{
					l.add(stringTokenizer.nextToken());
				}
				tvDecodeResult.setText("Username:"+l.get(0)+" \nPassword:"+l.get(1)+"\nVerified Successfully\nBeginning Transaction Phase.....");
				
			}
			new Handler().postDelayed(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					Intent i = new Intent(DecodeImageResult.this,LoginResult.class);
					i.putExtra("username", username);
					i.putExtra("password", password);
					startActivity(i);
				}
			},4000);
			
		}
		
	}
}
