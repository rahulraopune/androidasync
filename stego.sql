-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 03, 2016 at 07:18 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `stego`
--

-- --------------------------------------------------------

--
-- Table structure for table `auditlog`
--

CREATE TABLE IF NOT EXISTS `auditlog` (
`auditLogId` int(10) unsigned NOT NULL,
  `AC` varchar(45) NOT NULL,
  `payee` varchar(45) NOT NULL,
  `OldBalance` decimal(10,0) NOT NULL,
  `NewBalance` decimal(10,0) NOT NULL,
  `Amount` decimal(10,0) NOT NULL,
  `Action` varchar(1) NOT NULL,
  `updatedDate` datetime NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `auditlog`
--

INSERT INTO `auditlog` (`auditLogId`, `AC`, `payee`, `OldBalance`, `NewBalance`, `Amount`, `Action`, `updatedDate`) VALUES
(56, '1005', '1008', '108999', '109099', '100', 'C', '2012-06-09 12:07:35'),
(55, '1008', '1005', '812945', '812845', '100', 'D', '2012-06-09 12:07:35'),
(54, '1008', '1005', '807945', '812945', '5000', 'C', '2012-06-09 11:17:23'),
(53, '1005', '1008', '113999', '108999', '5000', 'D', '2012-06-09 11:17:23'),
(52, '1006', '1008', '800001', '800556', '555', 'C', '2012-06-08 19:49:35'),
(51, '1008', '1006', '808500', '807945', '555', 'D', '2012-06-08 19:49:35'),
(50, '1005', '1008', '113899', '113999', '100', 'C', '2012-06-08 19:48:56'),
(49, '1008', '1005', '808600', '808500', '100', 'D', '2012-06-08 19:48:56'),
(48, '1008', '1005', '807600', '808600', '1000', 'C', '2012-06-08 13:20:37'),
(47, '1005', '1008', '114899', '113899', '1000', 'D', '2012-06-08 13:20:37'),
(46, '1008', '1005', '807500', '807600', '100', 'C', '2012-06-08 13:19:13'),
(45, '1005', '1008', '114999', '114899', '100', 'D', '2012-06-08 13:19:13'),
(44, '1005', '1008', '109999', '114999', '5000', 'C', '2012-06-08 11:15:11'),
(43, '1008', '1005', '812500', '807500', '5000', 'D', '2012-06-08 11:15:11'),
(42, '1006', '1005', '800000', '800001', '1', 'C', '2012-06-08 10:46:57'),
(41, '1005', '1006', '110000', '109999', '1', 'D', '2012-06-08 10:46:57');

-- --------------------------------------------------------

--
-- Table structure for table `useraccounts`
--

CREATE TABLE IF NOT EXISTS `useraccounts` (
`uid` int(10) unsigned NOT NULL,
  `accountNo` varchar(45) NOT NULL,
  `active` varchar(1) NOT NULL DEFAULT 'Y',
  `fname` varchar(45) NOT NULL,
  `lname` varchar(45) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `UserId` varchar(45) NOT NULL,
  `uPassword` varchar(100) NOT NULL,
  `emailId` varchar(45) NOT NULL,
  `balance` decimal(10,0) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=35 ;

--
-- Dumping data for table `useraccounts`
--

INSERT INTO `useraccounts` (`uid`, `accountNo`, `active`, `fname`, `lname`, `phone`, `UserId`, `uPassword`, `emailId`, `balance`) VALUES
(31, '1005', 'Y', 'Komal', 'm', '9762309579', 'komal', 'komal', 'komaliphone@gmail.com', '42944'),
(34, '1007', 'Y', 'Priyanka', 'p', '8793111782', 'priyanka', 'priyanka', 'priyanka@gmail.com', '592155'),
(28, '1006', 'Y', 'yash', 'pt', '9021558990', 'yash', 'yash', 'yash@gmail.com', '783650'),
(29, '1008', 'Y', 'abc', 'xyz', '9021558990', 'abc', 'abc', 'abc@gmail.com', '812850');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auditlog`
--
ALTER TABLE `auditlog`
 ADD PRIMARY KEY (`auditLogId`);

--
-- Indexes for table `useraccounts`
--
ALTER TABLE `useraccounts`
 ADD PRIMARY KEY (`uid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auditlog`
--
ALTER TABLE `auditlog`
MODIFY `auditLogId` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `useraccounts`
--
ALTER TABLE `useraccounts`
MODIFY `uid` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
