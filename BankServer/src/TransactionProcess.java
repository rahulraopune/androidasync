

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 * Servlet implementation class TransactionProcess
 */
public class TransactionProcess extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TransactionProcess() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String inituser = request.getParameter("init");
		int creditamount = Integer.parseInt(request.getParameter("creditamount"));
		String  credituserid = request.getParameter("credituserid");
		String transactionfilename = request.getParameter("transactionfilename");
		System.out.println("------------------"+credituserid+" "+creditamount+" "+transactionfilename);
		File f = new File(ServerConfig.TEMP_PATH+transactionfilename+".jpg");
		try {
			
			Stegnography.encode(f.getAbsolutePath(),f.getAbsolutePath(), SimpleCryptoAndroidJava.encryptString(credituserid+"&"+creditamount));
			PrintWriter out = response.getWriter();
			String res = SimpleCryptoAndroidJava.decryptString(Stegnography.decode(ServerConfig.TRANSACTION_PATH+transactionfilename+".jpg", ServerConfig.TEMP_PATH+transactionfilename+".jpg"));
			DataAccessObject.addition(credituserid, creditamount);
			DataAccessObject.subtraction(inituser, creditamount);
			out.print(res);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
