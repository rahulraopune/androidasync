

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.BitSet;
import java.util.HashMap;

import javax.imageio.ImageIO;




public class Stegnography {
	
	//public static String inImg=ServerConstants.inImg;
	//public static String outImg=ServerConstants.outImg;
	public static String FILE_DIR="";
	static HashMap<String,String> hmpEncode;
	static HashMap<String,String> hmpDecode;
	static int count=64;
	static {
		hmpEncode=new HashMap<>();
		hmpDecode=new HashMap<>();
		hmpEncode.put("A","0");
		hmpEncode.put("B","1");
		hmpEncode.put("C","2");
		hmpEncode.put("D","3");
		hmpEncode.put("E","4");
		hmpEncode.put("F","5");
		hmpEncode.put("G","6");
		hmpEncode.put("H","7");
		hmpEncode.put("I","8");
		hmpEncode.put("J","9");
		hmpEncode.put("K","10");
		hmpEncode.put("L","11");
		hmpEncode.put("M","12");
		hmpEncode.put("N","13");
		hmpEncode.put("O","14");
		hmpEncode.put("P","15");
		hmpEncode.put("Q","16");
		hmpEncode.put("R","17");
		hmpEncode.put("S","18");
		hmpEncode.put("T","19");
		hmpEncode.put("U","20");
		hmpEncode.put("V","21");
		hmpEncode.put("W","22");
		hmpEncode.put("X","23");
		hmpEncode.put("Y","24");
		hmpEncode.put("Z","25");
		hmpEncode.put("a","26");
		hmpEncode.put("b","27");
		hmpEncode.put("c","28");
		hmpEncode.put("d","29");
		hmpEncode.put("e","30");
		hmpEncode.put("f","31");
		hmpEncode.put("g","32");
		hmpEncode.put("h","33");
		hmpEncode.put("i","34");
		hmpEncode.put("j","35");
		hmpEncode.put("k","36");
		hmpEncode.put("l","37");
		hmpEncode.put("m","38");
		hmpEncode.put("n","39");
		hmpEncode.put("o","40");
		hmpEncode.put("p","41");
		hmpEncode.put("q","42");
		hmpEncode.put("r","43");
		hmpEncode.put("s","44");
		hmpEncode.put("t","45");
		hmpEncode.put("u","46");
		hmpEncode.put("v","47");
		hmpEncode.put("w","48");
		hmpEncode.put("x","49");
		hmpEncode.put("y","50");
		hmpEncode.put("z","51");
		hmpEncode.put("0","52");
		hmpEncode.put("1","53");
		hmpEncode.put("2","54");
		hmpEncode.put("3","55");
		hmpEncode.put("4","56");
		hmpEncode.put("5","57");
		hmpEncode.put("6","58");
		hmpEncode.put("7","59");
		hmpEncode.put("8","60");
		hmpEncode.put("9","61");
		hmpEncode.put("=","62");
		hmpEncode.put("&","63");
		hmpDecode.put("0","A");
		hmpDecode.put("1","B");
		hmpDecode.put("2","C");
		hmpDecode.put("3","D");
		hmpDecode.put("4","E");
		hmpDecode.put("5","F");
		hmpDecode.put("6","G");
		hmpDecode.put("7","H");
		hmpDecode.put("8","I");
		hmpDecode.put("9","J");
		hmpDecode.put("10","K");
		hmpDecode.put("11","L");
		hmpDecode.put("12","M");
		hmpDecode.put("13","N");
		hmpDecode.put("14","O");
		hmpDecode.put("15","P");
		hmpDecode.put("16","Q");
		hmpDecode.put("17","R");
		hmpDecode.put("18","S");
		hmpDecode.put("19","T");
		hmpDecode.put("20","U");
		hmpDecode.put("21","V");
		hmpDecode.put("22","W");
		hmpDecode.put("23","X");
		hmpDecode.put("24","Y");
		hmpDecode.put("25","Z");
		hmpDecode.put("26","a");
		hmpDecode.put("27","b");
		hmpDecode.put("28","c");
		hmpDecode.put("29","d");
		hmpDecode.put("30","e");
		hmpDecode.put("31","f");
		hmpDecode.put("32","g");
		hmpDecode.put("33","h");
		hmpDecode.put("34","i");
		hmpDecode.put("35","j");
		hmpDecode.put("36","k");
		hmpDecode.put("37","l");
		hmpDecode.put("38","m");
		hmpDecode.put("39","n");
		hmpDecode.put("40","o");
		hmpDecode.put("41","p");
		hmpDecode.put("42","q");
		hmpDecode.put("43","r");
		hmpDecode.put("44","s");
		hmpDecode.put("45","t");
		hmpDecode.put("46","u");
		hmpDecode.put("47","v");
		hmpDecode.put("48","w");
		hmpDecode.put("49","x");
		hmpDecode.put("50","y");
		hmpDecode.put("51","z");
		hmpDecode.put("52","0");
		hmpDecode.put("53","1");
		hmpDecode.put("54","2");
		hmpDecode.put("55","3");
		hmpDecode.put("56","4");
		hmpDecode.put("57","5");
		hmpDecode.put("58","6");
		hmpDecode.put("59","7");
		hmpDecode.put("60","8");
		hmpDecode.put("61","9");
		hmpDecode.put("62","=");
		hmpDecode.put("63","&");

	}
	public static boolean encodeLSB(String inImg, String outImg, String msg){
		//spaces are ignored to improve encoding efficiency
//		msg = msg.toLowerCase().replaceAll(" ","");
		if(msg.length() == 0){
//			errText.setText("Empty message not allowed");
			System.err.println("Must encode non-empty message");
			return false;
		}
		try{

//		Bitmap immutbleImage=BitmapFactory.decodeFile(inImg);
//		Bitmap im = immutbleImage.copy(Bitmap.Config.ARGB_8888, true);
//		immutbleImage=null;
//		System.out.println("Starting Encoding... "+inImg+"  "+im.isMutable());
			File infile = new File(inImg); 
			BufferedImage im = 
			ImageIO.read(infile);
			 int width = im.getWidth();
		        int height = im.getHeight();
		       String msgnew="";
		       
		       System.out.println("Encoding... "+inImg +" width "+width+" Height "+height) ;       
		        for(int index = 0; index < msg.length(); ++index){
		        	int i=msg.charAt(index);
		        	String msgbinary=Integer.toBinaryString(i);
		        	if(msgbinary.length()<8){
		        		for(int j=msgbinary.length();j<8;j++){
		        			msgbinary="0"+msgbinary;
		        		}
		        	}
		        	if(msgbinary.length()>8){
		        		msgbinary=msgbinary.substring(msgbinary.length()-8);
		        	}
		        	
		        	System.out.print(msgbinary+"+");
		        	msgnew+=msgbinary;
		        }
		        msgnew+="0000000000000000";
		//        System.out.println(msgnew);
		        int in=0;
			for (int x = 0; x < width; x++){
				  for ( int y = 0; y < height; y++){
					   int pixel = im.getRGB(x,y);
		                int  red = (pixel >> 16) & 0xff;
		                int green = (pixel >> 8) & 0xff;
		                int blue = (pixel) & 0xff;
					  		if((in+2)<msgnew.length()){
					  			
					  		
			             
			            	int bit=msgnew.charAt(in) -'0';
			            	red=(bit==0)?red&0xFE:red|0x01;
			            	bit=msgnew.charAt(in+1) -'0';
			            	green=(bit==0)?green&0xFE:green|0x01;
			            	bit=msgnew.charAt(in+2) -'0';
			            	blue=(bit==0)?blue&0xFE:blue|0x01;
			            	in=in+3;
//			            	Color c=new Color();
//			            	c.rgb(red, green, blue)
			            	int rgb=pixel;
			            	rgb=rgb|(red<<16)&0x00FF0000;
			            	rgb=rgb|(green<<8)&0x0000FF00;
			            	rgb=rgb|(blue)&0x000000FF;
			            		im.setRGB(x, y, rgb);
					  		}
					  		else{
					  	 		im.setRGB(x, y, pixel);
					  		}
					 
				  }
			}
			                
			                

			System.out.println("Encoding complete.");

			File outfile = new File(outImg);
			
			ImageIO.write(im, extension(outImg), outfile);
//			save(outImg, im, content);
			
			return true;
		
		}catch(Exception ioe){

			ioe.printStackTrace();
			System.err.println("Io exception");
			return false;
		}
//		return false;

	}
	
	public static String decodeLSB(String inImg, String outImg){
		//spaces are ignored to improve encoding efficiency
		  String msgnew="";
		try{
			System.out.println("Starting Decoding...");
			
			File infile = new File(inImg); 
//			Bitmap im=BitmapFactory.decodeFile(inImg);
			BufferedImage im = ImageIO.read(infile);
			 int width = im.getWidth();
		        int height = im.getHeight();
		     
		       int ind=0;
BitSet bit=new BitSet(width*height);
			int zeros=0;
			for (int x = 0; x < width; x++){
				  for ( int y = 0; y < height; y++){
			                int pixel = im.getRGB(x,y);
			                int  red = (pixel >> 16) & 0xFF;
			                int green = (pixel >> 8) &0xFF;
			                int blue = (pixel) & 0xFF;
			                if(red==green&&green==blue&&green==0){
			                	zeros=zeros+3;
			                }else{
			                	zeros=0;
			                }
			                if(zeros>14)
			                	break;
			                
			                bit.set(ind,red==0?false:true);
			                bit.set(ind+1,green==0?false:true);
			                bit.set(ind+2,blue==0?false:true);
			                ind=ind+3;
			               
				  }
			}
			System.out.println();
//            for(int i=0;i<bit.length();i++){
//            	System.out.print(bit.get(i)?"1":"0");
//            	if(i%100==0)
//            		System.out.println();
//            }
			
							int j=7;
							byte b=0x0;
			                for(int i=0;i<bit.length();i++)
			                {
			                
			                	byte test=0x0;
			                	if(bit.get(i))
			                		test=0x1;
			                	b=(byte) (b|(test<<j));
			                	if(j==0){
				                	String str=Integer.toBinaryString(b);
				                		for(int u=str.length();u<8;u++)
				                			str="0"+str;
				                		
				                			//System.out.print(str+"+");
//				                		System.out.print((char) b);;
				                		msgnew+=(char) b;
				                		b=0x0;
				                		j=8;
				                	}
			                	j--;			                	
			                }
			                
			                String str=Integer.toBinaryString(b);
	                		for(int u=str.length();u<8;u++)
	                			str="0"+str;
	                			//System.out.print(str+"+");
	                			msgnew+=(char) b;

			System.out.println(" ---- "+msgnew+"--");
			System.out.println("Decoding complete.");

			return msgnew;
		
		}catch(Exception ioe){
//			errText.setText("IO Exception, possibly missing file");
			System.err.println("Io exception");
			return msgnew;
		}
//		return false;

	}
	
	
	public static boolean encode(String inImg, String outImg, String msg){
		//spaces are ignored to improve encoding efficiency
		msg = msg.toLowerCase().replaceAll(" ","");
		if(msg.length() == 0){
//			errText.setText("Empty message not allowed");
			System.err.println("Must encode non-empty message");
			return false;
		}
		try{
			System.out.println("Starting Encoding...");
//			Bitmap im=BitmapFactory.decodeFile(inImg);
			File infile = new File(inImg); 
			BufferedImage im = ImageIO.read(infile);
			
			int colIndex = im.getWidth() / count;
			int rowIndices = im.getHeight() / msg.length();			
			
			for(int index = 0; index < msg.length(); ++index){
				int row = rowIndices * index + (int) (Math.random() * (rowIndices - 1));
//				System.out.println(msg.charAt(index) +"  "+hmpEncode.get(msg.charAt(index)+""));
				String key=hmpEncode.get(msg.charAt(index)+"").toString();
			//	key=key!=null?key:"0";
				int val=new Integer(key).intValue();
				int col = ((val) * colIndex) + ((int) (Math.random() * (colIndex-1)));
				
			
				if(col < 0 || col > im.getWidth()){
					//only letters are encoded000000000000000000000000000
					System.err.println("Invalid (non-alpha) characters in message");
					return false;
				}
				
				if(im.getRGB(col,row)%2 == 0){
					im.setRGB(col,row,im.getRGB(col,row)+1);
				}else{
					im.setRGB(col,row,im.getRGB(col,row)-1);
				}
				
			}
						
//			System.out.println("Encoding complete.");
//			save(outImg, im, content);
			File outfile = new File(outImg);
			ImageIO.write(im, extension(outImg), outfile);
			return true;
		}
		catch(Exception ioe){
//			errText.setText("IO Exception, possibly missing file");
			System.err.println("Io exception"+ioe.getMessage());
			ioe.printStackTrace();
			return false;
		}

	}
	
	//method to extract a hidden message from stego object also given cover
	public static String decode(String orgImg, String codeImg){
		String msg = "";
		try{
			System.out.println("Starting decoding...");
			
			File plainFile = new File(orgImg); 
			BufferedImage plain = 
			ImageIO.read(plainFile);
			
			File codedFile = new File(codeImg); 
			BufferedImage coded = 
			ImageIO.read(codedFile);
			
			if(!(coded.getWidth() == plain.getWidth() && coded.getHeight() == plain.getHeight())){
//				errText.setText("Incompatible images for decode");
				System.err.println("Incompatible images");
				return msg;
			}
						
			int colIndex = coded.getWidth() / count;
			
			for(int row = 0; row < coded.getHeight(); ++row){
				for(int col = 0; col < coded.getWidth(); ++col){
					if(plain.getRGB(col,row) != coded.getRGB(col,row)){
						
						int key=(int) (col/colIndex);
					
						String value=hmpDecode.get(key+"").toString();
						System.out.println("key  "+ key+" value "+value);

						msg += value;
					}
				}
			}
			
			System.out.println("Decoding complete. "+msg);
						
			return msg;
		}
		catch(IOException ioe){
//			errText.setText("IO Exception, possibly missing file");
			System.err.println("Io exception");
		}
		return msg;
	}
	
	/*public static String getData(String str){
		if(str.indexOf("999999")!=-1){	    	
			str=str.substring(str.indexOf("999999")+"999999".length(),
					str.lastIndexOf("999999"));	
			try {
				System.out.println(str);																																																																																																																				Stegnography.encode(Stegnography.inImg, Stegnography.outImg, str);
				str=Stegnography.decode( Stegnography.outImg, Stegnography.inImg);
				str=SimpleCryptoAndroidJava.decryptString(str);
				str=str.trim();
				System.out.println("Decrypted String is "+str);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return str;
	}*/
	public static String extension(String fileName){
		return "png";
		
	}	


/*	public static void main(String[] args) {
		int i = '&';

		try {
			System.out.println(SimpleCryptoAndroidJava
					.encryptString("name=anup"));
			Stegnography
					.encode("D:/work/Internet Banking Using Stegnography & Session Handling/code/step1/sample/Winter.jpg",
							"D:/work/Internet Banking Using Stegnography & Session Handling/code/step1/sample/Winter_o.jpg",
							"7042655c0c6b220cf252429e75235ec5fb7a155048060bb12566f9409f45a7b1");
			System.out.println(Stegnography.decode(Stegnography.outImg,
					Stegnography.inImg));
			;

			String messageDecoded = Stegnography.decode(Stegnography.inImg,
					Stegnography.outImg);
			System.out.println("messageDecoded " + messageDecoded);
			messageDecoded = SimpleCryptoAndroidJava
					.decryptString(messageDecoded);
			System.out.println("messageDecoded " + messageDecoded);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}*/	
}