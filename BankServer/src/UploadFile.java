

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class UploadFile
 */
public class UploadFile extends HttpServlet {
	private static final long serialVersionUID = 1L;
	static final int BUFFER_SIZE = 4096;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UploadFile() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	/*protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		 if(ServletFileUpload.isMultipartContent(request)){
	            try {
	                List<FileItem> multiparts = new ServletFileUpload(
	                                         new DiskFileItemFactory()).parseRequest(request);
	              
	                for(FileItem item : multiparts){
	                    if(!item.isFormField()){
	                        String name = new File(item.getName()).getName();
	                        item.write( new File(UPLOAD_PATH + File.separator + name));
	                    }
	                }
	           
	               //File uploaded successfully
	                
	                out.print("File Uploaded Successfully");
	               //request.setAttribute("message", "");
	            } catch (Exception ex) {
	            	 out.print("Fail");
	              // request.setAttribute("message", "File Upload Failed due to " + ex);
	            }          
	         
	        }else{
	            out.print("Sorry this Servlet only handles file upload request");
	                                
	        }
	    
	        
	     
	    }*/
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String fileName = request.getHeader("fileName");
        File saveFile = new File(ServerConfig.UPLOAD_PATH + fileName);

        // prints out all header values
        System.out.println("===== Begin headers =====");
        Enumeration<String> names = request.getHeaderNames();
        while (names.hasMoreElements()) {
            String headerName = names.nextElement();
            System.out.println(headerName + " = " + request.getHeader(headerName));
        }
        System.out.println("===== End headers =====\n");

        // opens input stream of the request for reading data
        InputStream inputStream = request.getInputStream();

        // opens an output stream for writing file
        FileOutputStream outputStream = new FileOutputStream(saveFile);

        byte[] buffer = new byte[BUFFER_SIZE];
        int bytesRead = -1;
        System.out.println("Receiving data...");

        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }

        System.out.println("Data received.");
        outputStream.close();
        inputStream.close();
        
        File stego = new File(ServerConfig.TRANSACTION_PATH+fileName);
        
        Files.copy(saveFile.toPath(),stego.toPath());
        System.out.println("File written to: " + saveFile.getAbsolutePath());
        int base = stego.getAbsolutePath().lastIndexOf("/");
        String basename = stego.getAbsolutePath().substring(base+1,stego.getAbsolutePath().length());
        int ext = basename.lastIndexOf(".");
        String username = basename.substring(0, ext);
        System.out.println("USERNAME");
        String secretmessage;
		try
		{
			secretmessage = SimpleCryptoAndroidJava.encryptString(username+"&"+DataAccessObject.queryPassword(username));
			Stegnography.encode(stego.getAbsolutePath(), stego.getAbsolutePath(),secretmessage);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        
        
        // sends response to client
        
        
        response.getWriter().print("UPLOAD DONE");
        
	}

	

	}


