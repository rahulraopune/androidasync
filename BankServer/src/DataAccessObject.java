import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DataAccessObject
{
	public static final String DB_NAME ="stego";
	public static final String DB_PASSWD ="";
	public static final String DB_URL ="jdbc:mysql://localhost:3306/stego";
	
	
	public static Connection connection = null;
	public static Statement statement = null;
	public static PreparedStatement preparedStatement = null;
	
	
	public static Connection connect() throws ClassNotFoundException, SQLException
	{
		Class.forName("com.mysql.jdbc.Driver");
		connection = DriverManager.getConnection(DB_URL, "root", "");
		return connection;
	}
	
	public static Account login(Account account) throws Exception
	{
		connection = connect();
		String sql = "SELECT * FROM useraccounts WHERE UserId=? AND uPassword=?";
		preparedStatement = connection.prepareStatement(sql);
		preparedStatement.setString(1,account.getUserId());
		preparedStatement.setString(2,account.getuPassword());
		ResultSet resultSet = preparedStatement.executeQuery();
		Account a = new Account();
		while(resultSet.next()) 
		{
			a.setUid(resultSet.getInt("uid"));
			a.setAccountNo(resultSet.getInt("accountNo"));
			a.setFname(resultSet.getString("fname"));
			a.setLname(resultSet.getString("lname"));
			a.setPhone(resultSet.getString("phone"));
			a.setUserId(resultSet.getString("UserId"));
			a.setuPassword(SimpleCryptoAndroidJava.encryptString(resultSet.getString("uPassword")));
			a.setEmailID(resultSet.getString("emailId"));
			a.setBalance(resultSet.getInt("balance"));
		
			
		}
		System.out.println(a.toString());
		return a;
	}
	
	public static String queryPassword(String username) throws ClassNotFoundException, SQLException
	{
		String pass = "" ;
		connection = connect();
		String sql = "SELECT uPassword FROM useraccounts WHERE UserId=?";
		preparedStatement = connection.prepareStatement(sql);
		preparedStatement.setString(1,username);
		ResultSet resultSet = preparedStatement.executeQuery();
		while(resultSet.next())
		{
			pass = resultSet.getString("uPassword");
		}
		return pass;
	}
	
	public static void addition(String username,int amt) throws ClassNotFoundException, SQLException
	{
		
		connection = connect();
		String sql = "UPDATE useraccounts SET balance = balance+? WHERE UserId=?";
		preparedStatement = connection.prepareStatement(sql);
		preparedStatement.setInt(1,amt);
		preparedStatement.setString(2, username);
		preparedStatement.executeUpdate();
	}
	
	public static void subtraction(String username,int amt) throws ClassNotFoundException, SQLException
	{
	
		connection = connect();
		String sql = "UPDATE useraccounts SET balance = balance-? WHERE UserId=?";
		preparedStatement = connection.prepareStatement(sql);
		preparedStatement.setInt(1,amt);
		preparedStatement.setString(2, username);
		preparedStatement.executeUpdate();
	}
}
